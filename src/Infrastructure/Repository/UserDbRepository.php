<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Repository;

use fedor108\Repo\Domain\User;

class UserDbRepository implements UserRepositoryInterface
{
    public function __construct(
        private $db,
    ) {
    }

    public function getById(int $id): ?User
    {
        // TODO: Implement getById() method.
    }
}
