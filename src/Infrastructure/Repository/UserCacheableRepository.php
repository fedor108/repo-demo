<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Repository;

use fedor108\Repo\Domain\User;
use fedor108\Repo\Infrastructure\Cache\ModelFromCacheBuilderInterface;
use fedor108\Repo\Infrastructure\Cache\RedisCacheItemBuilderInterface;
use fedor108\Repo\Infrastructure\Cache\RedisCacheKeyGeneratorInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

class UserCacheableRepository implements UserRepositoryInterface
{
    public function __construct(
        private readonly UserRepositoryInterface $repository,
        private readonly CacheItemPoolInterface $cacheItemPool,
        private readonly RedisCacheKeyGeneratorInterface $cacheKeyGenerator,
        private readonly ModelFromCacheBuilderInterface $userFromCacheBuilder,
        private readonly RedisCacheItemBuilderInterface $cacheItemBuilder,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getById(int $id): ?User
    {
        $key = $this->cacheKeyGenerator->generate((string) $id);
        if ($this->cacheItemPool->hasItem($key)) {
            $cacheItem = $this->cacheItemPool->getItem($key);
            $user = $this->userFromCacheBuilder->build($cacheItem);
        } else {
            $user = $this->repository->getById($id);
            $cacheItem = $this->cacheItemBuilder->build($user);
            $this->cacheItemPool->save($cacheItem);
            $this->cacheItemPool->commit();
        }

        return $user;
    }
}
