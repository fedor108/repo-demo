<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Repository;

use fedor108\Repo\Domain\User;

interface UserRepositoryInterface
{
    public function getById(int $id): ?User;
}
