<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Cache;

use Psr\Cache\CacheItemInterface;

interface RedisCacheItemBuilderInterface
{
    public function build(mixed $value): CacheItemInterface;
}
