<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Cache;

use Psr\Cache\CacheItemInterface;

interface ModelFromCacheBuilderInterface
{
    public function build(CacheItemInterface $cacheItem): mixed;
}
