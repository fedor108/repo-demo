<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Cache;

Interface RedisCacheKeyGeneratorInterface
{
    public function generate(string $id, string $class = null): string;
}
