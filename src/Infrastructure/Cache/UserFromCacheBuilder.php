<?php

declare(strict_types=1);

namespace fedor108\Repo\Infrastructure\Cache;

use fedor108\Repo\Domain\User;
use Psr\Cache\CacheItemInterface;

class UserFromCacheBuilder implements ModelFromCacheBuilderInterface
{

    public function build(CacheItemInterface $cacheItem): mixed
    {
        return User::fromArray($cacheItem->toArray());
    }
}
